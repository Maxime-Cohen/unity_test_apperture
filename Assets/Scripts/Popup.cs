﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    public enum Type {Image=0, Model}
    
    public ListProject listProject;

    public GameObject addProject;
    public GameObject deleteProject;

    private Type _typeNewProject;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    public void HidePopup()
    {
        ResetPopupContent();
        gameObject.SetActive(false);
    }

    public void ResetPopupContent()
    {
        RadioButton[] buttons = gameObject.GetComponentsInChildren<RadioButton>();
        Array.ForEach(buttons,button => button.Deactivate());
        InputField inputField = gameObject.GetComponentInChildren<InputField>();
        if (inputField)
        {
            inputField.text = "";
        }
    }

    public void ShowPopupAddProject(int type)
    {
        _typeNewProject = (Type)type;
        gameObject.SetActive(true);
        addProject.SetActive(true);
        deleteProject.SetActive(false);
    }
    
    public void ShowPopupDeleteProject()
    {
        gameObject.SetActive(true);
        addProject.SetActive(false);
        deleteProject.SetActive(true);
    }

    public void CreateNewProject()
    {
        InputField inputField = gameObject.GetComponentInChildren<InputField>();
        if (inputField)
        {
            if (_typeNewProject == Type.Image)
            {
                listProject.AddProjectImage(inputField.text);
            }
            else
            {
                listProject.AddProjectModel(inputField.text);
            }
        }

        HidePopup();
    }
}
