﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Project : MonoBehaviour
{
    public GameObject deleteWindow;
    public Image image;
    public Text title;
    public ListProject.SortType type;

    public void SetImage(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void SetTitle(string title)
    {
        this.title.text = title;
    }

    public bool IsType(ListProject.SortType t)
    {
        return (type == t || t == ListProject.SortType.All);
    }

    public Button GetDeleteButton()
    {
        return deleteWindow.GetComponentInChildren<Button>();
    }

    public void SetDeleteWindowActive(bool state)
    {
        deleteWindow.SetActive(state);
    }
    
}
