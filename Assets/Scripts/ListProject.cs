﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ListProject : MonoBehaviour
{
    public enum SortType
    {
        All,
        Image,
        Model
    }
    
    public Sprite[] images;
    
    public GameObject prefabImage;
    public GameObject prefabModel;
    public GameObject projectContent;
    public Popup popup;

    private List<GameObject> _projects;
    private RectTransform _projectContentTransform;

    private Vector2 _projectSize;

    private SortType _displaySort;
    private int _projectToDeleteId;
    private bool _projectDeleteMode;

    // Start is called before the first frame update
    void Start()
    {
        _projects = new List<GameObject>();
        _projectContentTransform = projectContent.GetComponent<RectTransform>();
        _projectSize = prefabImage.GetComponent<RectTransform>().rect.size;
        _displaySort = SortType.All;
        DisplayProject();
        SetDeleteProjectMode(false);
    }
    
    public void AddProjectImage(string pName)
    {
        var newProject = Instantiate(prefabImage, projectContent.transform) as GameObject;
        Project projectScript = newProject.GetComponent<Project>();
        projectScript.SetImage(images[Random.Range(0, images.Length - 1)]);
        projectScript.SetTitle(pName);
        projectScript.GetDeleteButton().onClick.AddListener(() => SetProjectToDeleteId(newProject.GetInstanceID()));
        projectScript.GetDeleteButton().onClick.AddListener(popup.ShowPopupDeleteProject);
        projectScript.SetDeleteWindowActive(false);
        _projects.Add(newProject);
        DisplayProject();
    }
    
    public void AddProjectModel(string pName)
    {
        var newProject = Instantiate(prefabModel, projectContent.transform) as GameObject;
        Project projectScript = newProject.GetComponent<Project>();
        projectScript.SetTitle(pName);
        projectScript.GetDeleteButton().onClick.AddListener(() => SetProjectToDeleteId(newProject.GetInstanceID()));
        projectScript.GetDeleteButton().onClick.AddListener(popup.ShowPopupDeleteProject);
        projectScript.SetDeleteWindowActive(false);
        _projects.Add(newProject);
        DisplayProject();
    }

    public void SetProjectToDeleteId(int pId)
    {
        _projectToDeleteId = pId;
    }

    public void SetDeleteProjectMode(bool state)
    {
        _projectDeleteMode = state;
        _projects.ForEach(project => project.GetComponent<Project>().SetDeleteWindowActive(state));
    }

    public void ToggleProjectDeleteMode()
    {
        SetDeleteProjectMode(!_projectDeleteMode);
    }

    public void DeleteProject()
    {
        _projects.ForEach(p => Debug.Log(p.GetInstanceID()));
        GameObject project = _projects.Find(p => p.GetInstanceID() == _projectToDeleteId);
        _projects.Remove(project);
        Destroy(project);
        DisplayProject();
    }
    

    public void DisplayProject()
    {
        int nbProjectByLine = (int) (_projectContentTransform.rect.width / _projectSize.x);
        
        int i = 0;
        foreach (var project in _projects)
        {
            if (project.GetComponent<Project>().IsType(_displaySort))
            {
                project.GetComponent<RectTransform>().anchoredPosition = new Vector2(
                    i % nbProjectByLine * _projectSize.x, i / nbProjectByLine * -_projectSize.y);
                project.SetActive(true);
                ++i;
            }
            else
            {
                project.SetActive(false);
            }
        }
        
        int nbLine = (int) Math.Ceiling((decimal) i / nbProjectByLine);
        Rect rect = _projectContentTransform.rect;
        _projectContentTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _projectSize.y * nbLine);
    }

    public void DisplayAllProject()
    {
        _displaySort = SortType.All;
        DisplayProject();
    }
    
    public void DisplayImageProject()
    {
        _displaySort = SortType.Image;
        DisplayProject();
    }
    
    public void DisplayModelProject()
    {
        _displaySort = SortType.Model;
        DisplayProject();
    }
}
