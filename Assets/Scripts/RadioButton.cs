﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadioButton : MonoBehaviour
{
    public Sprite spriteActive;
    public Color colorButtonActive;
    public Color colorTextActive;
    public Sprite spriteDeactive;
    public Color colorButtonDeactive;
    public Color colorTextDeactive;
    public bool check;

    private Text _text;
    private Image _image;
    
    // Start is called before the first frame update
    void Start()
    {
        _image = gameObject.GetComponent<Image>();
        _text = gameObject.GetComponentInChildren<Text>();
        if (check)
        {
            Activate();
        }
        else
        {
            Deactivate();
        }
    }

    public void Activate()
    {
        if (spriteActive)
        {
            _image.sprite = spriteActive;
        }
        else
        {
            _image.color = colorButtonActive;
        }
        _text.color = colorTextActive;
    }
    
    public void Deactivate()
    {
        if (spriteActive)
        {
            _image.sprite = spriteDeactive;
        }
        else
        {
            _image.color = colorButtonDeactive;
        }
        _text.color = colorTextDeactive;
    }

    public void Check()
    {
        check = true;
        Activate();
    }
    
    public void Uncheck()
    {
        check = false;
        Deactivate();
    }

    public bool IsCheck()
    {
        return check;
    }

    public void Toggle()
    {
        if (check)
        {
            Uncheck();
        }
        else
        {
            Check();
        }
    }
}
