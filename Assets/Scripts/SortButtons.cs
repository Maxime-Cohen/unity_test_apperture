﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SortButtons : MonoBehaviour
{
    public RadioButton[] sortButtons;

    public void SortBy(int sort)
    {
        for (int i = 0; i < sortButtons.Length; i++)
        {
            if (sort == i)
            {
                sortButtons[i].Activate();
            }
            else
            {
                sortButtons[i].Deactivate();
            }
        }
    }
}
